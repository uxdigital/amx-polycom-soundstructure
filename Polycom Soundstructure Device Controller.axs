MODULE_NAME='Polycom Soundstructure Device Controller' (DEV controller, DEV device)
(***********************************************************)
(*  FILE CREATED ON: 10/08/2014  AT: 11:11:28              *)
(***********************************************************)
(*  FILE_LAST_MODIFIED_ON: 02/25/2015  AT: 22:32:49        *)
(***********************************************************)
(*******************************************************************************)
(*                                                                             *)
(*                   � Control Designs Software Ltd (2012)                     *)
(*                         www.controldesigns.co.uk                            *)
(*                                                                             *)
(*      Tel: +44 (0)1753 208 490     Email: support@controldesigns.co.uk       *)
(*                                                                             *)
(*******************************************************************************)
(*                                                                             *)
(*                 Polycom Soundstructure Device Controller                    *)
(*                                                                             *)
(*            Written by Mike Jobson (Control Designs Software Ltd)            *)
(*                                                                             *)
(** REVISION HISTORY ***********************************************************)
(*                                                                             *)
(*  Please see README.md included with this project                            *)
(*                                                                             *)
(*******************************************************************************)
(*                                                                             *)
(*  Permission is hereby granted, free of charge, to any person obtaining a    *)
(*  copy of this software and associated documentation files (the "Software"), *)
(*  to deal in the Software without restriction, including without limitation  *)
(*  the rights to use, copy, modify, merge, publish, distribute, sublicense,   *)
(*  and/or sell copies of the Software, and to permit persons to whom the      *)
(*  Software is furnished to do so, subject to the following conditions:       *)
(*                                                                             *)
(*  The above copyright notice and this permission notice shall be included in *)
(*  all copies or substantial portions of the Software.                        *)
(*                                                                             *)
(*  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS    *)
(*  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF                 *)
(*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.     *)
(*  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY       *)
(*  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT  *)
(*  OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR   *)
(*  THE USE OR OTHER DEALINGS IN THE SOFTWARE.                                 *)
(*                                                                             *)
(*******************************************************************************)


(*******************************************************************************)
(*  IMPORT CORE LIBRARY HERE                                                   *)
(*  This is includes generic functions and code which can be re-used in main   *)
(*  and other modules. Also includes 'SNAPI' and some add-on functions.        *)
(*******************************************************************************)
#DEFINE CORE_LIBRARY
//#DEFINE DEBUG
#INCLUDE 'Core Library'
#INCLUDE 'Core Debug'
#INCLUDE 'SNAPI'

DEFINE_CONSTANT

INTEGER MAX_VIRTUAL_CHANNELS = 50

INTEGER MAX_DEFINITION_NAME_LENGTH			= 40
INTEGER MAX_DEFINITION_TYPE_LENGTH			= 40

DEFINE_TYPE

STRUCT _VIRTUAL_CHANNEL {
    INTEGER id
    INTEGER defined
    CHAR name[MAX_DEFINITION_NAME_LENGTH]
    CHAR type[MAX_DEFINITION_TYPE_LENGTH]
    SINTEGER faderValue
    SINTEGER faderMin
    SINTEGER faderMax
    INTEGER fader
    INTEGER bool
    DEVCHAN controllerChannel
    DEVLEV controllerLevel
    INTEGER dialerIndex
    INTEGER dialerStatus
}

DEFINE_VARIABLE

_VIRTUAL_CHANNEL virtualChannels[MAX_VIRTUAL_CHANNELS]

VOLATILE INTEGER stringBufferBusy = FALSE
VOLATILE CHAR stringBuffer[10000] = ''

VOLATILE programChannelLoop = 0

DEFINE_FUNCTION VChannelInit(_VIRTUAL_CHANNEL vc) {
    vc.defined = 0
    vc.id = 0
    vc.name = ''
    vc.type = ''
    vc.faderValue = 0
    vc.faderMin = -100
    vc.faderMax = 20
    vc.fader = 0
    vc.bool = 0
    vc.controllerChannel = {controller, 0}
    vc.controllerLevel = {controller, 0}
    vc.dialerIndex = 0
    vc.dialerStatus = 0
}

DEFINE_FUNCTION VChannelInitWithID(_VIRTUAL_CHANNEL vc, INTEGER id) {
    VChannelInit(vc)
    vc.id = id
}

DEFINE_FUNCTION VChannelInitAll() {
    STACK_VAR INTEGER n
    
    for(n = 1; n <= MAX_LENGTH_ARRAY(virtualChannels); n ++) {
	VChannelInitWithID(virtualChannels[n], n)
    }
}

DEFINE_FUNCTION INTEGER VChannelID(CHAR channelName[]) {
    STACK_VAR INTEGER n
    
    for(n = 1; n <= MAX_LENGTH_ARRAY(virtualChannels); n ++) {
	if(virtualChannels[n].name == channelName) {
	    return virtualChannels[n].id
	}
    }
    
    return 0
}

DEFINE_FUNCTION INTEGER VChannelIndexForID(INTEGER id) {
    STACK_VAR INTEGER n
    
    for(n = 1; n <= MAX_LENGTH_ARRAY(virtualChannels); n ++) {
	if(virtualChannels[n].id == id) {
	    return n
	}
    }
    
    return 0
}

DEFINE_FUNCTION INTEGER VChannelIndexForName(CHAR channelName[]) {
    STACK_VAR INTEGER n
    
    for(n = 1; n <= MAX_LENGTH_ARRAY(virtualChannels); n ++) {
	if(virtualChannels[n].name == channelName) {
	    return n
	}
    }
    
    return 0
}

DEFINE_FUNCTION CHAR[MAX_DEFINITION_NAME_LENGTH] VChannelNameForDialer(INTEGER dialerIndex) {
    STACK_VAR INTEGER n
    
    for(n = 1; n <= MAX_LENGTH_ARRAY(virtualChannels); n ++) {
	if(virtualChannels[n].dialerIndex == dialerIndex && dialerIndex) {
	    return virtualChannels[n].name
	}
    }
    
    return ''
}

DEFINE_FUNCTION INTEGER VChannelDialerStatus(CHAR channelName[]) {
    STACK_VAR INTEGER channelIndex
    
    channelIndex = VChannelIndexForName(channelName)
    
    if(!channelIndex) return 0
    
    return virtualChannels[channelIndex].dialerStatus
}

DEFINE_FUNCTION INTEGER VChannelRegister(CHAR channelName[], CHAR channelType[], INTEGER levelValue, INTEGER channelValue, INTEGER dialerIndex) {
    STACK_VAR INTEGER n
    
    n = VChannelIndexForName(channelName)
    
    if(n) {
	virtualChannels[n].type = channelType
	virtualChannels[n].controllerLevel.LEVEL = levelValue
	virtualChannels[n].controllerChannel.CHANNEL = channelValue
	virtualChannels[n].dialerIndex = dialerIndex
	VChannelRequestUpdate(channelName)
	return virtualChannels[n].id
    }
    
    for(n = 1; n <= MAX_LENGTH_ARRAY(virtualChannels); n ++) {
	if(!virtualChannels[n].defined) {
	    virtualChannels[n].defined = TRUE
	    virtualChannels[n].name = channelName
	    virtualChannels[n].type = channelType
	    virtualChannels[n].controllerLevel.LEVEL = levelValue
	    virtualChannels[n].controllerChannel.CHANNEL = channelValue
	    virtualChannels[n].dialerIndex = dialerIndex
	    virtualChannels[n].dialerStatus = 0
	    VChannelRequestUpdate(channelName)
	    return virtualChannels[n].id
	}
    }
    
    return 0
}

DEFINE_FUNCTION VChannelRequestUpdate(CHAR channelName[]) {
    STACK_VAR INTEGER channelIndex
    
    if([controller, DEVICE_COMMUNICATING]) {
	if(LENGTH_STRING(channelName)) {
	    channelIndex = VChannelIndexForName(channelName)
	    if(channelIndex) {
		DeviceSend("'get fader min "', virtualChannels[channelIndex].name, '"'")
		DeviceSend("'get fader max "', virtualChannels[channelIndex].name, '"'")
		DeviceSend("'get fader "', virtualChannels[channelIndex].name, '"'")
		DeviceSend("'get mute "', virtualChannels[channelIndex].name, '"'")
	    }
	}
    }
}

DEFINE_FUNCTION VChannelFaderValueSet(CHAR channelName[], SINTEGER value) {
    STACK_VAR INTEGER channelIndex
    
    channelIndex = VChannelIndexForName(channelName)
    
    if(channelIndex) {
	virtualChannels[channelIndex].faderValue = value
	virtualChannels[channelIndex].fader = TYPE_CAST(ScaleRange(virtualChannels[channelIndex].faderValue, virtualChannels[channelIndex].faderMin, virtualChannels[channelIndex].faderMax, 0 ,255))
	if(virtualChannels[channelIndex].controllerLevel.LEVEL) {
	    SEND_COMMAND controller, "'LEVEL_CHANGE-"', channelName, '",', itoa(virtualChannels[channelIndex].controllerLevel.LEVEL), ',', itoa(virtualChannels[channelIndex].fader)"
	    SEND_LEVEL virtualChannels[channelIndex].controllerLevel, virtualChannels[channelIndex].fader
	}
    }
}

DEFINE_FUNCTION VChannelFaderSet(CHAR channelName[], INTEGER value) {
    STACK_VAR INTEGER channelIndex
    
    channelIndex = VChannelIndexForName(channelName)
    
    if(channelIndex) {
	DeviceSend("'set fader "', virtualChannels[channelIndex].name, '" ', itoa(ScaleRange(value, 0, 255, virtualChannels[channelIndex].faderMin, virtualChannels[channelIndex].faderMax))")
    }
}

DEFINE_FUNCTION VChannelFaderSetDB(CHAR channelName[], SINTEGER value) {
    STACK_VAR INTEGER channelIndex
    
    channelIndex = VChannelIndexForName(channelName)
    
    if(channelIndex) {
	DeviceSend("'set fader "', virtualChannels[channelIndex].name, '" ', itoa(value)")
    }
}

DEFINE_FUNCTION VChannelMuteSet(CHAR channelName[], INTEGER mute) {
    STACK_VAR INTEGER channelIndex
    
    channelIndex = VChannelIndexForName(channelName)
    
    if(channelIndex) {
	virtualChannels[channelIndex].bool = mute
	DeviceSend("'set mute "', virtualChannels[channelIndex].name, '" ', itoa(mute)")
    }
}

DEFINE_FUNCTION DeviceSend(CHAR str[]) {
    DebugSendStringToConsole("'SS Tx: ', str")
    SEND_STRING device, "str, $0D"
}

DEFINE_FUNCTION DeviceHandshake() {
    DeviceSend('get eth_mac 1')
    DeviceSend('get dev_type 1')
    DeviceSend('get dev_firmware_ver 1')
}

DEFINE_START

CREATE_BUFFER device, stringBuffer


DEFINE_EVENT

CHANNEL_EVENT[controller, 0] {
    ON: {
	STACK_VAR INTEGER channelIndex
	
	for(channelIndex = 1; channelIndex <= MAX_LENGTH_ARRAY(virtualChannels); channelIndex ++) {
	    if(virtualChannels[channelIndex].controllerChannel.CHANNEL && virtualChannels[channelIndex].controllerChannel.CHANNEL == channel.channel) {
		if(!virtualChannels[channelIndex].bool) {
		    VChannelMuteSet(virtualChannels[channelIndex].name, TRUE)
		}
		break
	    }
	}
    }
    OFF: {
	STACK_VAR INTEGER channelIndex
	
	for(channelIndex = 1; channelIndex <= MAX_LENGTH_ARRAY(virtualChannels); channelIndex ++) {
	    if(virtualChannels[channelIndex].controllerChannel.CHANNEL && virtualChannels[channelIndex].controllerChannel.CHANNEL == channel.channel) {
		if(virtualChannels[channelIndex].bool) {
		    VChannelMuteSet(virtualChannels[channelIndex].name, FALSE)
		}
		break
	    }
	}
    }
}

DATA_EVENT[controller] {
    ONLINE: {
	VChannelInitAll()
	ON[controller, DATA_INITIALIZED]
    }
    COMMAND: {
	_SNAPI_DATA snapi
	
	SNAPI_InitDataFromDevice(snapi, data.device, data.text)
	
	switch(snapi.cmd) {
	    case 'REINIT': {
		OFF[controller, DATA_INITIALIZED]
		OFF[controller, DEVICE_COMMUNICATING]
		VChannelInitAll()
		ON[controller, DATA_INITIALIZED]
		DeviceHandshake()
	    }
	    case 'CHANNEL_REGISTER': {
		VChannelRegister(snapi.param[1], '', atoi(snapi.param[2]), atoi(snapi.param[3]), atoi(snapi.param[4]))
	    }
	    case 'CHANNEL_UPDATE': {
		VChannelRequestUpdate(snapi.param[1])
	    }
	    case 'CHANNEL_FADER_SET': {
		VChannelFaderSet(snapi.param[1], atoi(snapi.param[2]))
	    }
	    case 'CHANNEL_FADER_SET_DB': {
		VChannelFaderSetDB(snapi.param[1], atoi(snapi.param[2]))
	    }
	    case 'CHANNEL_FADER_INC': {
		STACK_VAR INTEGER stepVal
		
		stepVal = atoi(snapi.param[2])
		if(!stepVal) stepVal = 1
		
		DeviceSend("'inc fader "', snapi.param[1], '" ', itoa(stepVal)")
	    }
	    case 'CHANNEL_FADER_DEC': {
		STACK_VAR INTEGER stepVal
		
		stepVal = atoi(snapi.param[2])
		if(!stepVal) stepVal = 1
		
		DeviceSend("'dec fader "', snapi.param[1], '" ', itoa(stepVal)")
	    }
	    case 'PRESET': {
		DeviceSend("'run "', snapi.param[1], '"'")
	    }
	    case 'DIAL': {
		STACK_VAR INTEGER dialerIndex
		STACK_VAR CHAR channelName[MAX_DEFINITION_NAME_LENGTH]
		
		dialerIndex = atoi(snapi.param[1])
		channelName = VChannelNameForDialer(dialerIndex)
		
		if(!VChannelDialerStatus(channelName)) {
		    DeviceSend("'set phone_connect "', channelName, '" 1'")
		}
		
		DeviceSend("'set phone_dial "', channelName, '" "', snapi.param[2], '"'")
	    }
	    case 'ANSWER': {
		STACK_VAR INTEGER dialerIndex
		STACK_VAR CHAR channelName[MAX_DEFINITION_NAME_LENGTH]
		
		dialerIndex = atoi(snapi.param[1])
		channelName = VChannelNameForDialer(dialerIndex)
		
		DeviceSend("'set phone_connect "', channelName, '" 1'")
	    }
	    case 'REJECT': {
		STACK_VAR INTEGER n
		STACK_VAR INTEGER dialerIndex
		STACK_VAR CHAR channelName[MAX_DEFINITION_NAME_LENGTH]
		
		dialerIndex = atoi(snapi.param[1])
		channelName = VChannelNameForDialer(dialerIndex)
		
		DeviceSend("'set phone_connect "', channelName, '" 1'")
		for(n = 1; n <= 10000; n ++) {}
		DeviceSend("'set phone_connect "', channelName, '" 0'")
	    }
	    case 'DISCONNECT': {
		STACK_VAR INTEGER dialerIndex
		STACK_VAR CHAR channelName[MAX_DEFINITION_NAME_LENGTH]
		
		dialerIndex = atoi(snapi.param[1])
		channelName = VChannelNameForDialer(dialerIndex)
		
		DeviceSend("'set phone_connect "', channelName, '" 0'")
	    }
	    case 'PASSTHRU': {
		DeviceSend(snapi.param[1])
	    }
	}
    }
}

DATA_EVENT[device] {
    ONLINE: {
	SEND_COMMAND data.device, 'SET MODE DATA'
	SEND_COMMAND data.device, 'HSOFF'
	SEND_COMMAND data.device, 'SET BAUD 38400,N,8,1 485 DISABLE'
	
	wait 10 {
	    if(![controller, DEVICE_COMMUNICATING]) {
		DeviceHandshake()
	    }
	}
    }
    STRING: {
	STACK_VAR CHAR line[255]
	STACK_VAR CHAR temp[255]
	STACK_VAR CHAR commandString[255]
	STACK_VAR CHAR channelName[255]
	STACK_VAR INTEGER channelIndex
	STACK_VAR CHAR value[255]
	
	if(!stringBufferBusy) {
	    stringBufferBusy = TRUE
	    
	    while(LENGTH_STRING(stringBuffer) && FIND_STRING(stringBuffer, "$0D", 1)) {
		line = REMOVE_STRING(stringBuffer, "$0D", 1)
		
		DebugSendStringToConsole("'SS Rx: ', line")
		
		// Found eth_mac
		if(FIND_STRING(line, 'val eth_mac 1 "', 1)) {
		    temp = StringFromTwoDelimiters(line, 'val eth_mac 1 "', '"', 1)
		    SEND_COMMAND controller, "'MAC_ADDRESS-', temp"
		}
		
		// Found dev_type
		else if(FIND_STRING(line, 'val dev_type 1 ', 1)) {
		    temp = StringFromTwoDelimiters(line, 'val dev_type 1 ', "$0D", 1)
		    SEND_COMMAND controller, "'DEV_TYPE-', UPPER_STRING(temp)"
		}
		
		// Found firmware_ver
		else if(FIND_STRING(line, 'val dev_firmware_ver 1 "', 1)) {
		    temp = StringFromTwoDelimiters(line, 'val dev_firmware_ver 1 "', '"', 1)
		    SEND_COMMAND controller, "'FWVERSION-', temp"
		    ON[controller, DEVICE_COMMUNICATING]
		}
		
		// Found other val
		else if(FIND_STRING(line, 'val ', 1)) {
		    if(FIND_STRING(line, ' "', 1)) {
			commandString = StringFromTwoDelimiters(line, 'val ', ' "', 1)
			channelName = StringFromTwoDelimiters(line, ' "', '" ', 1)
			value = StringFromTwoDelimiters(line, '" ', "$0D", 1)
		    } else {
			commandString = StringFromTwoDelimiters(line, 'val ', ' ', 1)
			channelName = StringFromTwoDelimiters(line, "commandString, ' '", ' ', 1)
			value = StringFromTwoDelimiters(line, "commandString, ' ', channelName, ' '", "$0D", 1)
		    }
		    
		    channelIndex = VChannelIndexForName(channelName)
		    
		    DebugAddDataToArray('SS Rx', 'commandString', commandString)
		    DebugAddDataToArray('SS Rx', 'channelName', channelName)
		    DebugAddDataToArray('SS Rx', 'value', value)
		    DebugAddNumberToArray('SS Rx', 'atoi(value)', atoi(value))
		    DebugSendArrayToConsole('SS Rx')
		    
		    if(channelIndex) {
			
			switch(commandString) {
			    case 'fader max': virtualChannels[channelIndex].faderMax = atoi(value)
			    case 'fader min': virtualChannels[channelIndex].faderMin = atoi(value)
			    case 'fader': {
				VChannelFaderValueSet(virtualChannels[channelIndex].name, atoi(value))
			    }
			    case 'mute': {
				virtualChannels[channelIndex].bool = atoi(value)
				[virtualChannels[channelIndex].controllerChannel] = virtualChannels[channelIndex].bool
			    }
			    case 'phone_connect': {
				virtualChannels[channelIndex].dialerStatus = atoi(value)
				SEND_COMMAND controller, "'DIALER_STATUS-', itoa(virtualChannels[channelIndex].dialerIndex), ',', itoa(virtualChannels[channelIndex].dialerStatus)"
			    }
			    case 'digital_gpio_state': {
				virtualChannels[channelIndex].bool = !(atoi(value))
				[virtualChannels[channelIndex].controllerChannel] = virtualChannels[channelIndex].bool
			    }
			}
		    } else {
			switch(commandString) {
			    case 'phone_ring': {
				if(value == '1') {
				    SEND_COMMAND controller, "'INCOMING_CALL-', channelName"
				}
			    }
			}
		    }
		}
	    }
	    
	    stringBufferBusy = FALSE
	}
    }
}

DEFINE_PROGRAM {
    programChannelLoop ++
    
    if(programChannelLoop > MAX_LENGTH_ARRAY(virtualChannels)) {
	programChannelLoop = 1
    }
    
    if(virtualChannels[programChannelLoop].defined && virtualChannels[programChannelLoop].controllerChannel.CHANNEL) {
	[virtualChannels[programChannelLoop].controllerChannel] = (virtualChannels[programChannelLoop].bool)
    } else if(!virtualChannels[programChannelLoop].defined) {
	programChannelLoop = 0
    }
}